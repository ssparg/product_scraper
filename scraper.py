import re
from datetime import datetime
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from webdriver_manager.chrome import ChromeDriverManager
from database import Database


products = {
    'Samsung 7kg Washer and 5kg Dryer Washing Machine': {
        1: {'shop': 'Takealot', 'url': 'https://www.takealot.com/samsung-7kg-washer-5kg-dryer-silver/PLID51856054'},
        2: {'shop': 'Makro', 'url': 'https://www.makro.co.za/appliances/washers-tumble-dryers/washing-machine-dryer-set/washer-dryer-combo/samsung-7-kg-5-kg-washing-machine-dryer-combo-/p/000000000000320143_EA'}
    },
    'Midea - 6.5 Litre Countertop Dishwasher': {
        1: {'shop': 'Takealot', 'url': 'https://www.takealot.com/midea-6-5-litre-countertop-dishwasher-white/PLID52806610'}
    },
    'Swiss Dishwasher White 6 Plate': {
        1: {'shop': 'Makro', 'url': 'https://www.makro.co.za/appliances/washers-tumble-dryers/dishwashers/dishwashers/swiss-dishwasher-white-6-plate---counter-top/p/60030944-a8c7-456d-858a-d0a4cab46792'}
    },
    'Samsung - 8Kg Front Loader Washing Machine': {
        1: {'shop': 'Takealot', 'url': 'https://www.takealot.com/samsung-8kg-front-loader-washing-machine-ww80j5555fx/PLID70628505'}
    },
    'Samsung Frost Free Combi Fridge & Freezer With Water Dispenser (321L)': {
        1: {'shop': 'Takealot', 'url': 'https://www.takealot.com/samsung-frost-free-combi-fridge-freezer-with-water-dispenser/PLID70627410'}
    },
    'Samsung 360 l Combi Fridge/Freezer with Water Dispenser': {
        1: {'shop': 'Makro', 'url': 'https://www.makro.co.za/appliances/fridges-freezers/fridges/combination-fridge-freezer/samsung-360-l-combi-fridge-freezer-with-water-dispenser-/p/000000000000347360_EA'}
    },
    'Retractaline - Pluto - Gull Winged Clothes Dryer - 18 meters': {
        1: {'shop': 'Takealot', 'url': 'https://www.takealot.com/retractaline-pluto-gull-winged-clothes-dryer-18-meters/PLID51970462'},
        2: {'shop': 'Makro', 'url': 'https://www.makro.co.za/home-garden/laundry/clothes-drying-hanging/drying-equipment/retractaline-pluto-wire-dryer-/p/000000000002198924_EA'}
    },
    'Samsung 58" Display Crystal Processor 4KUHD TV': {
        1: {'shop': 'Takealot', 'url': 'https://www.takealot.com/samsung-58-display-crystal-processor-4kuhd-tv/PLID69348406'},
        2: {'shop': 'Makro', 'url': 'https://www.makro.co.za/electronics-computers/televisions/led/-46-117cm-/samsung-147-cm-58-smart-uhd-tv-/p/000000000000395020_EA'}
    }
}

shop_price_elements = {
    'Takealot': {
        1: {'type':'class', 'name': 'currency'}
    },
    'Makro': {
        1: {'type':'class', 'name': 'price'}
    }
}

today = datetime.today().strftime('%Y-%m-%d')

def initialize(db):
    shops = []
    db.execute('''
        CREATE TABLE IF NOT EXISTS shops
        (id INTEGER PRIMARY KEY AUTOINCREMENT,
        name VARCHAR NOT NULL UNIQUE);
    ''')

    db.execute('''
        CREATE TABLE IF NOT EXISTS products
        (id INTEGER PRIMARY KEY AUTOINCREMENT,
        name VARCHAR NOT NULL,
        shop_id INTEGER NOT NULL,
        FOREIGN KEY(shop_id) REFERENCES shops(id));
    ''')

    db.execute('''
        CREATE TABLE IF NOT EXISTS product_urls
        (id INTEGER PRIMARY KEY AUTOINCREMENT,
        product_id INTEGER NOT NULL,
        url TEXT NOT NULL UNIQUE,
        FOREIGN KEY(product_id) REFERENCES products(id));
    ''')

    db.execute('''
        CREATE TABLE IF NOT EXISTS prices
        (product_id INTEGER NOT NULL,
        date DATE NOT NULL,
        price REAL NOT NULL,
        PRIMARY KEY (product_id, date)
        FOREIGN KEY(product_id) REFERENCES products(id));
    ''')
    
    for product in products:        
        for shop in products[product]:            
            if products[product][shop]['shop'] not in shops:
                db.execute("INSERT INTO shops (name) VALUES (?)", (products[product][shop]['shop'],))
                shop_id = db.lastid()
                shops.append(products[product][shop]['shop'])
            else:
                shop_id = shops.index(products[product][shop]['shop']) + 1

            db.execute('''INSERT INTO products (name, shop_id) VALUES (?, ?)''', (product, shop_id))
            product_id = db.lastid()
            db.execute("INSERT INTO product_urls (product_id, url) VALUES (?, ?)", (product_id, products[product][shop]['url']))
      
def scrape(driver, shop, url):
    price = 0,00
    try:
        driver.get(url)
        element = get_element(driver, shop)
        price = element.text;
        price = re.findall(r"[-+]?\d*\,\d+|\d+", price)[0]
    finally:
        return price

def get_element(driver, shop):
    if shop not in shop_price_elements:
        raise Exception("{} not found in elements dict".format(shop))
    
    shop_elements = shop_price_elements[shop][1]

    if shop_elements['type'] == 'class': #add on as we need!
        element = WebDriverWait(driver, 10).until(
                    EC.presence_of_element_located((By.CLASS_NAME, shop_elements['name']))
                )
    return element


with Database('scraper.sqlite') as db:
    tables = db.query("SELECT name FROM sqlite_master WHERE type='table'")

    if (len(tables) == 0):
        #Fresh script run so let's set up our tables
        initialize(db)
        
    
    products = db.query('''SELECT products.id AS id, products.name AS product, shops.name AS shop, url 
                           FROM products
                           JOIN product_urls ON product_urls.product_id = products.id
                           JOIN shops ON shops.id = products.shop_id
                        ''')

    if (len(products) > 0):
        options = webdriver.ChromeOptions()
        options.add_argument("--headless")
        options.add_argument("--log-level=3")
        options.add_argument("user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.75 Safari/537.36")
        driver = webdriver.Chrome(ChromeDriverManager().install(), options=options)
        
        for product in products:
            price = scrape(driver, product['shop'], product['url'])
            db.execute("INSERT OR IGNORE INTO prices (product_id, date, price) VALUES (?, ?, ?)", (product['id'], today, price))

        driver.close()
        print("Done!")


            
        
                
                
            


                
        
        
    
