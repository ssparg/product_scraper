from database import Database

with Database('scraper.sqlite') as db:
    prices = db.query('''SELECT products.name, price, date, shops.name AS shop
                         FROM products
                         JOIN shops ON shops.id = products.shop_id
                         JOIN prices ON prices.product_id = products.id''')
    for price in prices:
        print("{} on {} at {} cost {}".format(price['name'], price['date'], price['shop'], price['price']))